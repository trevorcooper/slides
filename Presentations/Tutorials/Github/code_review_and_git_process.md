title: Code Review & Git Process
author:
    name: "Trevor Cooper"
    twitter: "@HPCDevOPS"
output: code_review_and_git_process.html
theme: tcooper/cleaver-retro
controls: false

--

# Code Review & Git Process
## With a Practical Example

--

### Code Review... (what)?

SDSC GitHub has ~50-60 'Private' repos that were migrated from CVS.

--

### Code Review... (what)?

SDSC GitHub has ~50-60 'Private' repos that were migrated from CVS.

Most (all?) of these were build-tested during Trestles upgrade.

--

### Code Review... (what)?

SDSC GitHub has ~50-60 'Private' repos that were migrated from CVS.

Most (all?) of these were build-tested during Trestles upgrade.

Most of these will be make **public** after code review.

--

### Code Review... (why)?

Some of the SDSC private repos **may** have 'secrets' in them that should not
be released.

--

### Code Review... (why)?

Some of the SDSC private repos **may** have 'secrets' in them that should not
be released.

Example... 

--

### Code Review... (why)?

Some of the SDSC private repos **may** have 'secrets' in them that should not
be released.

Example... gordon-config-roll

--

### Code Review... (why)?

Some of the SDSC private repos **may** have 'secrets' in them that should not
be released.

Example... gordon-config-roll

**(DEMO)**

--

### Code Review... (why)?

Some of the SDSC private repos **may** have 'secrets' in them that should not
be released.

Example... gordon-config-roll

In order to completely remove these secrets the Git commit history **must** 
be rewritten, the repository **must** be **DELETED** and **ALL** forks/clones 
**must** be removed.

--

### Code Review... (who)?

	lux:gordon-config-roll tcooper$ git-summary

	 project  : gordon-config-roll
	 repo age : 2 years, 3 months
	 active   : 72 days
	 commits  : 195
	 files    : 97
	 authors  : 
		71	Jerry Greeenberg        36.4%
		62	Christopher Irving      31.8%
		44	Rick Wagner             22.6%
		13	Trevor Cooper           6.7%
		 4	Eva Hocks               2.1%
		 1	Jim Hayes               0.5%

--

### Code Review... (when)?

As Soon As Possible?

--

### Git Process - Two 'Roles'

There are two basic roles in the realm of management of our source 
repositories...

--

### Git Process - Two 'Roles'

There are two basic roles in the realm of management of our source 
repositories...

- Contributor

--

### Git Process - Two 'Roles'

There are two basic roles in the realm of management of our source 
repositories...

- Contributor
- Maintainer

--

### Git Process - Two 'Roles'

There are two basic roles in the realm of management of our source 
repositories...

- Contributor
- Maintainer

Since we **all** have push access to SDSC repositories we can **all** perform
 **both** these functions.

--

### Git Process - Tools

I use two git add-ons pretty extensively. I have tried to indicate where they
are used in this tutorial.

--

### Git Process - Tools

I use two git add-ons pretty extensively. I have tried to indicate where they
are used in this tutorial.

- <a href="https://github.com/visionmedia/git-extras" target="_blank">visionmedia/git-extras</a>
- <a href="http://hub.github.com" target="_blank">hub.github.com</a>

--

### Git Process - Tools

I use two git add-ons pretty extensively. I have tried to indicate where they
are used in this tutorial.

- <a href="https://github.com/visionmedia/git-extras" target="_blank">visionmedia/git-extras</a>
- <a href="http://hub.github.com" target="_blank">hub.github.com</a>

Consider adding them to your toolbelt to make your Git-ing easier...

--

### Git Process - Contributing

Example workflow for contributing to a project:

	$ git clone sdsc/gordon-config-roll
	$ cd gordon-config-roll

--

### Git Process - Contributing

Example workflow for contributing to a project:

	$ git clone sdsc/gordon-config-roll
	$ cd gordon-config-roll

**NOTE:** In some cases I've added more commands than needed to complete the 
steps just to  show what is going on.

--

### Git Process - Contributing

Don't forget your configuration.

--

### Git Process - Contributing

Don't forget your configuration.

When editing/commiting as 'you'...

	$ git config --global user.name "Your Name"
    $ git config --global user.email you@example.com

--

### Git Process - Contributing

Don't forget your configuration.

When editing/commiting as **root** on Cluster development node...

	$ git config user.name "Your Name"
    $ git config user.email you@example.com

--

### Git Process - Contributing

Don't forget your configuration.

When editing/commiting as **root** on Cluster development node...

	$ git config user.name "Your Name"
    $ git config user.email you@example.com

**NOTE:** Please build rolls as root, do not **commit** code as root.

--

### Git Process - Contributing

Create a 'feature' branch...

	$ git checkout -b feature
	
--

### Git Process - Contributing

Make your changes... then...
	
	$ git status
	  (display changed files ... )
	$ git diff <changed_file>
	  (review changes ... )
	$ git add <changed_file(s)>
	  (add files to staging area ... )
	$ git commit -m "Done with feature"
	  (commit changes ... )

--

### Git Process - Contributing

It's time to fork the repo using the GitHub Web UI...

- Browse to **upstream** repository
- Click the **Fork** button
- **Copy** the repository URL

--

### Git Process - Contributing

Add the new remote to your clone...

	$ git remote add YOUR_USER https://github.com/YOUR_USER/gordon-config-roll.git

--

### Git Process - Contributing

It's time to fork the repo using hub...

	$ hub fork
	 -> (forking repo on GitHub...)
	 -> git remote add YOUR_USER git://github.com/YOUR_USER/gordon-config-roll.git
 
Replace all that Web UI with a single command of two words... :)
 
--

### Git Process - Contributing

Push the changes to your new remote...
 
	$ git push YOUR_USER feature

--

### Git Process - Contributing

Open a pull request for the topic branch you've just pushed in the GitHub Web UI...

- Browse to **your** fork
- Click on **Compare & pull request**
- Edit commit message and click on **Send pull request**

--

### Git Process - Contributing

Alternate if **Compare & pull request** is not displayed...

- Select your **feature** branch
- Click on **Compare & review** button
- Click on **Click to create a pull request for this comparision**
- Edit commit message and click on **Send pull request**

--

### Git Process - Contributing

Open a pull request for the topic branch you've just pushed using hub...

	$ hub pull-request
	  -> (opens a text editor for your pull request message)

Once again... hub rules.

--

### Git Process - Maintaining

Browse issues in GitHub Web UI...

- Click on link in email (if you're configured to watch the repository)
- Browse to **upstream** repository
- Click on **Issues** icon

--

### Git Process - Maintaining

Browse issues using hub...

	$ hub browse -- issues

Need I say more?

--

### Git Process - Maintaining

Check out a pull request for review using *vanilla* git cli...

	$ git branch
	  (display branches ... )
	$ git checkout -b hocks-master master
	  (checkout a new branch to test the changes ... )
	$ git branch
	  (display branches ... )
	$ git pull https://github.com/hocks/gordon-config-roll.git master
	  (bring in hocks's changes and test ... )
	$ git log master..
	  (examine commit messages ... )
	$ git diff master..
	  (examine code changes ... )
	  (test code changes ... )

--

### Git Process - Maintaining

Merge changes from pull request/branch using *vanilla* git cli...

	$ git branch
	  (display branches ... )
	$ git checkout master
	  (switch to the 'master' branch ... )
	$ git merge hocks-master
	  (fast-forward merge of branch if possible ... )
	$ git log
	  (review commit log ... )
	$ git push origin master
	  (push changes to origin master ... )

--

### Git Process - Maintaining

Check out a pull request for review using hub...

	$ hub checkout https://github.com/sdsc/gordon-config-roll/pull/1
	  -> (creates a new branch with the contents of the pull request)
	$ git branch
	  (show which branch you are on ... )
	$ git log master..
	  (examine commit messages ... )
	$ git diff master..
	  (examine code changes ... )
	  (test code changes ... )

--

### Git Process - Maintaining

Merge changes from pull request/branch using *vanilla* git cli...

	$ git branch
	  (display branches ... )
	$ git checkout master
	  (switch to master branch ... )
	$ git merge hocks-master
	  (fast-forward merge of branch if possible ... )
	$ git log
	  (review commit log ... )
	$ git push origin master
	  (push changes to origin master ... )

--

### Git Process - Maintaining

You may wish to delete your remote...

	$ git remote -v
	  (show remotes ... )
	$ git remote rm hocks
	  (remote remote(s) ... )

**NOTE:** This step is not necessary but I'm showing you how.