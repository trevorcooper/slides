title: Merging a Pull Request
author:
    name: "Trevor Cooper"
    twitter: "@HPCDevOPS"
output: merging_a_pull_request.html
theme: tcooper/cleaver-retro
controls: false

--

# Merging a Pull Request
## Command Line Version

--

### Clone the 'master' repository

	$ cd github/sdsc/

	$ git clone https://github.com/sdsc/gordon-config-roll.git
	Cloning into 'gordon-config-roll'...
	remote: Reusing existing pack: 1085, done.
	remote: Total 1085 (delta 0), reused 0 (delta 0)
	Receiving objects: 100% (1085/1085), 12.32 MiB | 3.28 MiB/s, done.
	Resolving deltas: 100% (614/614), done.
	Checking connectivity... done

	$ cd gordon-config-roll
	$ git status
	# On branch master
	nothing to commit, working directory clean

--

### Check out a new branch to test the changes

	$ git checkout -b hocks-master master
	Switched to a new branch 'hocks-master'

--

### Bring in changes and test

	git pull https://github.com/hocks/gordon-config-roll.git master
	remote: Counting objects: 4, done.
	remote: Compressing objects: 100% (4/4), done.
	remote: Total 4 (delta 0), reused 2 (delta 0)
	Unpacking objects: 100% (4/4), done.
	From https://github.com/hocks/gordon-config-roll
	 * branch            master     -> FETCH_HEAD
	Updating 286fbd5..c271242
	Fast-forward
	 nodes/gordon-compute-torque.xml | 62 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++---
	 1 file changed, 59 insertions(+), 3 deletions(-)
 
--
 
### Review Changes (Optional)
 
	 $ git diff master hocks-master
	diff --git a/nodes/gordon-compute-torque.xml b/nodes/gordon-compute-torque.xml
	index 2764e53..e09c4f5 100644
	--- a/nodes/gordon-compute-torque.xml
	+++ b/nodes/gordon-compute-torque.xml
	@@ -27,14 +27,70 @@ $prologalarm 300
	 -:ALL EXCEPT root (wheel) (xsede-admin):ALL
	 </file>
 
	-chmod u+s /opt/torque/sbin/pbs_iff
	-
	 ## prologue and epilogue are propagated by 411 group Compute 1/16/12+1 EH

	-
	ln -s /var/spool/torque/mom_priv/prologue /var/spool/torque/mom_priv/prologue.parallel
	ln -s /var/spool/torque/mom_priv/epilogue /var/spool/torque/mom_priv/epilogue.parallel
 
	+
	+<file name="/etc/sysconfig/pbs_mom" mode="append">
	+echo "PBS_FLASH=&my_iser;
	+</file>
	+
	+
	+<file name="/etc/profile.d/scratchdir.sh" perms="755">
	+#!/bin/sh
	+# /etc/profile.d/scratchdir.sh
	+# If PBS_ENVIRONMENT exists and is "PBS_BATCH" or "PBS_INTERACTIVE",
	+# set SCRATCH_DIR
	+if [ -f /etc/sysconfig/pbs_mom ] ; then
	+    . /etc/sysconfig/pbs_mom
	+fi
	+
	+
	+if [ -n "$PBS_ENVIRONMENT" ]
	+then
	+  if [ "$PBS_ENVIRONMENT" = PBS_BATCH -o "$PBS_ENVIRONMENT" = PBS_INTERACTIVE ]
	+  then
	+    if [[ "$PBS_FLASH" == "noflash" ]] ; then
	+      export OASIS_SCRATCH=/oasis/scratch/$PBS_USER/$PBS_JOBID
	+      export LOCAL_SCRATCH=noflash 
	+      export ENVIRONMENT="BATCH"
	+    else
	+      export OASIS_SCRATCH=/oasis/scratch/$PBS_USER/$PBS_JOBID
	+      export LOCAL_SCRATCH=/scratch/$PBS_USER/$PBS_JOBID
	+      export ENVIRONMENT="BATCH"
	+    fi
	+  fi
	+fi
	+</file>
	+
	+
	+<file name="/etc/profile.d/scratchdir.csh" perms="755">
	+#!/bin/csh
	+# /etc/profile.d/tmpdir.csh
	+# If PBS_ENVIRONMENT exists and is "PBS_BATCH" or "PBS_INTERACTIVE",
	+# set SCRATCH_DIR
	+if ( -f /etc/sysconfig/pbs_mom ) then
	+    . /etc/sysconfig/pbs_mom
	+endif
	+
	+if ( $?PBS_ENVIRONMENT ) then


### Merge the changes and update the server

	$  git checkout master
	Switched to branch 'master'

	$ git merge hocks-master
	Updating 286fbd5..c271242
	Fast-forward
	 nodes/gordon-compute-torque.xml | 62 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++---
	 1 file changed, 59 insertions(+), 3 deletions(-)

	$ git status
	# On branch master
	# Your branch is ahead of 'origin/master' by 1 commit.
	#   (use "git push" to publish your local commits)
	#
	nothing to commit, working directory clean

	$ git push origin master

--

### Delete pull-request branch

	$ git branch -d hocks-master
	Deleted branch hocks-master (was c271242).

--

### Review History (Optional)

	$ git log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short
	lux:gordon-config-roll tcooper$ git log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short
	* c271242 2014-01-09 | add noflash logic to compute nodes (HEAD, master) [Eva]
	* 286fbd5 2013-11-08 | Update README.md (origin/master, origin/HEAD) [Trevor Cooper]
	* 26e79dc 2013-10-15 | Removed /home/servers/gordon/bin from path and replaced with /opt/sdsc/bin. [Trevor Cooper]
	...
	* 6b038cf 2011-10-21 | turning off services on Gordon compute nodes [Rick Wagner]
	* 8cd6e16 2011-10-21 | Work hacking ever. Nodes files to install custom built kernel and modules. [Rick Wagner]
	* bb18d19 2011-10-17 | Skeleton Gordon configuration roll. [Rick Wagner]
